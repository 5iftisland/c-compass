﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCompass
{
    public partial class DiaryForm : System.Windows.Forms.Form
    {

        private ExternalCommandData _ExternalCommandData;
        private Document _Document;
        private UIDocument _UIDocument;

        public DiaryForm(ExternalCommandData commandData)
        {
            InitializeComponent();

            _ExternalCommandData = commandData;
            _Document = _ExternalCommandData.Application.ActiveUIDocument.Document;
            _UIDocument = _ExternalCommandData.Application.ActiveUIDocument;


            label1.Text = ServiceActions.HttpGet(ServiceConfig.DIARY_FORM_SERVICE_URL);

        }

        private void DiaryForm_Load(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
