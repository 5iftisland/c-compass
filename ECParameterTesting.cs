﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCompass
{

    public class ECParameterTesting : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            // 跳出訊息視窗顯示版本資訊
            //TaskDialog.Show("DevMessage", "IA Lab. 2014/12/16/ iLuxProby N2");
            //選取物件
            UIDocument uiDoc = commandData.Application.ActiveUIDocument;
            Reference rElement = uiDoc.Selection.PickObject(ObjectType.Element, "選取開窗物件");

            //取得資訊
            Element rWindow = uiDoc.Document.GetElement(rElement);

            StringBuilder sb = new StringBuilder();
            foreach (Element element in elements)
            {
                sb.AppendLine(GetElementParameterInformation(commandData, element));
            }
            TaskDialog.Show("ParameterTesting", sb.ToString());
            return Result.Succeeded;
        }

        private String GetElementParameterInformation(ExternalCommandData commandData, Element element)
        {
            // Format the prompt information string
            String prompt = "Show parameters in selected Element:";
            StringBuilder st = new StringBuilder();
            // iterate element's parameters
            foreach (Parameter para in element.Parameters)
            {
                st.AppendLine(GetParameterInformation(para));
            }
            // Give the user some information
            return st.ToString();
        }

        String GetParameterInformation(Parameter para)
        {
            string defName = para.Definition.Name + @"\t";
            // Use different method to get parameter data according to the storage type
            switch (para.StorageType)
            {
                case StorageType.Double:
                    //covert the number into Metric
                    defName += " : " + para.AsValueString();
                    break;
                case StorageType.ElementId:
                    //find out the name of the element
                    ElementId id = para.AsElementId();
                    if (id.IntegerValue >= 0)
                    {

                    }
                    else
                    {

                    }
                    break;
                case StorageType.Integer:
                    if (ParameterType.YesNo == para.Definition.ParameterType)
                    {
                        if (para.AsInteger() == 0)
                        {
                            defName += " : " + "False";
                        }
                        else
                        {
                            defName += " : " + "True";
                        }
                    }
                    else
                    {
                        defName += " : " + para.AsInteger().ToString();
                    }
                    break;
                case StorageType.String:
                    defName += " : " + para.AsString();
                    break;
                default:
                    defName = "Unexposed parameter.";
                    break;
            }
            return defName;
        }
    }
}
