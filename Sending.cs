﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCompass
{
    public partial class Sending : System.Windows.Forms.Form
    {
        private ExternalCommandData _ExternalCommandData;
        private Document _Document;
        private UIDocument _UIDocument;

        public Sending(ExternalCommandData commandData)
        {
            InitializeComponent();

            _ExternalCommandData = commandData;
            _Document = _ExternalCommandData.Application.ActiveUIDocument.Document;
            _UIDocument = _ExternalCommandData.Application.ActiveUIDocument;
        }

        private void FormSample_Load(object sender, EventArgs e)
        {
            
        }
    }
}
