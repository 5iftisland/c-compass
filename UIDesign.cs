﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection; // 要加入 PresentationCore、System.Xaml
using System.Text;
using System.Windows.Media.Imaging;

namespace CCompass
{
    [Transaction(TransactionMode.Manual)]
    class UIDesign : IExternalApplication
    {
        UIDesignConfiguration uiDesignConfiguration = new UIDesignConfiguration();

        public Result OnStartup(UIControlledApplication application)
        {
            // Create a custom ribbon tab
            application.CreateRibbonTab(UIDesignConfiguration.UI_RIBBON_TAB);

            RibbonPanel panel = application.CreateRibbonPanel(UIDesignConfiguration.UI_RIBBON_TAB, UIDesignConfiguration.UI_PANEL_LOGO);
            addPushButtonData(panel,
                UIDesignConfiguration.UI_BTN_ID_LOGO,
                UIDesignConfiguration.UI_BTN_NAME_LOGO,
                uiDesignConfiguration.AssemblyFullName,
                UIDesignConfiguration.UI_BTN_EXECUTOR_LOGO
                , uiDesignConfiguration.UI_BTN_IMAGE_LOGO,
                UIDesignConfiguration.UI_BTN_HELP_LOGO,
                UIDesignConfiguration.UI_BTN_HELP_URL_LOGO);

            RibbonPanel panelModeling = application.CreateRibbonPanel(UIDesignConfiguration.UI_RIBBON_TAB, UIDesignConfiguration.UI_PANEL_Modeling);
            AddModeling(panelModeling);
            //panelModeling.AddSeparator();   //分隔線

            RibbonPanel panelDesigning = application.CreateRibbonPanel(UIDesignConfiguration.UI_RIBBON_TAB, UIDesignConfiguration.UI_PANEL_DESIGNING);
            AddDesigning(panelDesigning);

            RibbonPanel panelConstructing = application.CreateRibbonPanel(UIDesignConfiguration.UI_RIBBON_TAB, UIDesignConfiguration.UI_PANEL_CONSTRUCTING);
            AddConstructing(panelConstructing);

            RibbonPanel panelRecording = application.CreateRibbonPanel(UIDesignConfiguration.UI_RIBBON_TAB, UIDesignConfiguration.UI_PANEL_RECORDING);
            AddRecording(panelRecording);

            RibbonPanel panelReport = application.CreateRibbonPanel(UIDesignConfiguration.UI_RIBBON_TAB, UIDesignConfiguration.UI_PANEL_REPORT);
            AddReport(panelReport);


            return Result.Succeeded;
        }

        private void AddReport(RibbonPanel panel)
        {
            addPushButtonData(panel,
               UIDesignConfiguration.UI_BTN_ID_LIST,
               UIDesignConfiguration.UI_BTN_NAME_LIST,
               uiDesignConfiguration.AssemblyFullName,
               UIDesignConfiguration.UI_BTN_EXECUTOR_LIST,
               uiDesignConfiguration.UI_BTN_IMAGE_LIST,
               UIDesignConfiguration.UI_BTN_HELP_LIST,
               UIDesignConfiguration.UI_BTN_HELP_URL_LIST);

            addPushButtonData(panel,
               UIDesignConfiguration.UI_BTN_ID_STATEMENT,
               UIDesignConfiguration.UI_BTN_NAME_STATEMENT,
               uiDesignConfiguration.AssemblyFullName,
               UIDesignConfiguration.UI_BTN_EXECUTOR_STATEMENT,
               uiDesignConfiguration.UI_BTN_IMAGE_STATEMENT,
               UIDesignConfiguration.UI_BTN_HELP_STATEMENT,
               UIDesignConfiguration.UI_BTN_HELP_URL_STATEMENT);

            //addPushButtonData(panel,
            //   UIDesignConfiguration.UI_BTN_ID_STATEMENT,
            //   UIDesignConfiguration.UI_BTN_NAME_STATEMENT,
            //   uiDesignConfiguration.AssemblyFullName,
            //   UIDesignConfiguration.UI_BTN_EXECUTOR_STATEMENT,
            //   uiDesignConfiguration.UI_BTN_IMAGE_STATEMENT,
            //   UIDesignConfiguration.UI_BTN_HELP_STATEMENT,
            //   UIDesignConfiguration.UI_BTN_HELP_URL_STATEMENT);



            //AddSplitButton(panel, "SplitButtons01", "Split Buttons", getSettingPushButton(UIDesignConfiguration.UI_BTN_ID_DAILY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_NAME_DAILY_STATEMENT,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_DAILY_STATEMENT,
            //    uiDesignConfiguration.UI_BTN_IMAGE_DAILY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_DAILY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_DAILY_STATEMENT),
            //    getSettingPushButton(UIDesignConfiguration.UI_BTN_ID_WEEKLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_NAME_WEEKLY_STATEMENT,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_WEEKLY_STATEMENT,
            //    uiDesignConfiguration.UI_BTN_IMAGE_WEEKLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_WEEKLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_WEEKLY_STATEMENT),
            //    getSettingPushButton(UIDesignConfiguration.UI_BTN_ID_MONTHLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_NAME_MONTHLY_STATEMENT,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_MONTHLY_STATEMENT,
            //    uiDesignConfiguration.UI_BTN_IMAGE_MONTHLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_MONTHLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_MONTHLY_STATEMENT));

            addPushButtonData(panel,
              UIDesignConfiguration.UI_BTN_ID_REPORT,
              UIDesignConfiguration.UI_BTN_NAME_REPORT,
              uiDesignConfiguration.AssemblyFullName,
              UIDesignConfiguration.UI_BTN_EXECUTOR_REPORT,
              uiDesignConfiguration.UI_BTN_IMAGE_REPORT,
              UIDesignConfiguration.UI_BTN_HELP_REPORT,
              UIDesignConfiguration.UI_BTN_HELP_URL_REPORT);


            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_DAILY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_NAME_DAILY_STATEMENT,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_DAILY_STATEMENT,
            //    uiDesignConfiguration.UI_BTN_IMAGE_DAILY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_DAILY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_DAILY_STATEMENT);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_WEEKLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_NAME_WEEKLY_STATEMENT,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_WEEKLY_STATEMENT,
            //    uiDesignConfiguration.UI_BTN_IMAGE_WEEKLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_WEEKLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_WEEKLY_STATEMENT);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_MONTHLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_NAME_MONTHLY_STATEMENT,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_MONTHLY_STATEMENT,
            //    uiDesignConfiguration.UI_BTN_IMAGE_MONTHLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_MONTHLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_MONTHLY_STATEMENT);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_REPORT,
            //    UIDesignConfiguration.UI_BTN_NAME_REPORT,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_REPORT,
            //    uiDesignConfiguration.UI_BTN_IMAGE_REPORT,
            //    UIDesignConfiguration.UI_BTN_HELP_REPORT,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_REPORT);
        }

        private void AddRecording(RibbonPanel panel)
        {
            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_WEEKLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_NAME_WEEKLY_STATEMENT,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_WEEKLY_STATEMENT,
            //    uiDesignConfiguration.UI_BTN_IMAGE_WEEKLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_WEEKLY_STATEMENT,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_WEEKLY_STATEMENT);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_STATISTICS,
            //    UIDesignConfiguration.UI_BTN_NAME_STATISTICS,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_STATISTICS,
            //    uiDesignConfiguration.UI_BTN_IMAGE_STATISTICS,
            //    UIDesignConfiguration.UI_BTN_HELP_STATISTICS,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_STATISTICS);

            addPushButtonData(panel,
                UIDesignConfiguration.UI_BTN_ID_CHECKING_UPDATE,
                UIDesignConfiguration.UI_BTN_NAME_CHECKING_UPDATE,
                uiDesignConfiguration.AssemblyFullName,
                UIDesignConfiguration.UI_BTN_EXECUTOR_CHECKING_UPDATE,
                uiDesignConfiguration.UI_BTN_IMAGE_CHECKING_UPDATE,
                UIDesignConfiguration.UI_BTN_HELP_CHECKING_UPDATE,
                UIDesignConfiguration.UI_BTN_HELP_URL_CHECKING_UPDATE);

            addPushButtonData(panel,
                UIDesignConfiguration.UI_BTN_ID_PROGRESS,
                UIDesignConfiguration.UI_BTN_NAME_PROGRESS,
                uiDesignConfiguration.AssemblyFullName,
                UIDesignConfiguration.UI_BTN_EXECUTOR_PROGRESS,
                uiDesignConfiguration.UI_BTN_IMAGE_PROGRESS,
                UIDesignConfiguration.UI_BTN_HELP_PROGRESS,
                UIDesignConfiguration.UI_BTN_HELP_URL_PROGRESS);

            addPushButtonData(panel,
                UIDesignConfiguration.UI_BTN_ID_STORAGE,
                UIDesignConfiguration.UI_BTN_NAME_STORAGE,
                uiDesignConfiguration.AssemblyFullName,
                UIDesignConfiguration.UI_BTN_EXECUTOR_STORAGE,
                uiDesignConfiguration.UI_BTN_IMAGE_STORAGE,
                UIDesignConfiguration.UI_BTN_HELP_STORAGE,
                UIDesignConfiguration.UI_BTN_HELP_URL_STORAGE);
        }

        private void AddConstructing(RibbonPanel panel)
        {
            addPushButtonData(panel,
                UIDesignConfiguration.UI_BTN_ID_COMPOSE,
                UIDesignConfiguration.UI_BTN_NAME_COMPOSE,
                uiDesignConfiguration.AssemblyFullName,
                UIDesignConfiguration.UI_BTN_EXECUTOR_COMPOSE,
                uiDesignConfiguration.UI_BTN_IMAGE_COMPOSE,
                UIDesignConfiguration.UI_BTN_HELP_COMPOSE,
                UIDesignConfiguration.UI_BTN_HELP_URL_COMPOSE);

            addPushButtonData(panel,
                UIDesignConfiguration.UI_BTN_ID_SENDING,
                UIDesignConfiguration.UI_BTN_NAME_SENDING,
                uiDesignConfiguration.AssemblyFullName,
                UIDesignConfiguration.UI_BTN_EXECUTOR_SENDING,
                uiDesignConfiguration.UI_BTN_IMAGE_SENDING,
                UIDesignConfiguration.UI_BTN_HELP_SENDING,
                UIDesignConfiguration.UI_BTN_HELP_URL_SENDING);
        }

        private void AddDesigning(RibbonPanel panel)
        {

            addPushButtonData(panel,
                UIDesignConfiguration.UI_BTN_ID_METHODS,
                UIDesignConfiguration.UI_BTN_NAME_METHODS,
                uiDesignConfiguration.AssemblyFullName,
                UIDesignConfiguration.UI_BTN_EXECUTOR_METHODS,
                uiDesignConfiguration.UI_BTN_IMAGE_METHODS,
                UIDesignConfiguration.UI_BTN_HELP_METHODS,
                UIDesignConfiguration.UI_BTN_HELP_URL_METHODS);

            addPushButtonData(panel,
                UIDesignConfiguration.UI_BTN_ID_ASSEMBLING,
                UIDesignConfiguration.UI_BTN_NAME_ASSEMBLING,
                uiDesignConfiguration.AssemblyFullName,
                UIDesignConfiguration.UI_BTN_EXECUTOR_ASSEMBLING,
                uiDesignConfiguration.UI_BTN_IMAGE_ASSEMBLING,
                UIDesignConfiguration.UI_BTN_HELP_ASSEMBLING,
                UIDesignConfiguration.UI_BTN_HELP_URL_ASSEMBLING);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_TIME,
            //    UIDesignConfiguration.UI_BTN_NAME_TIME,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_TIME,
            //    uiDesignConfiguration.UI_BTN_IMAGE_TIME,
            //    UIDesignConfiguration.UI_BTN_HELP_TIME,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_TIME);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_COST,
            //    UIDesignConfiguration.UI_BTN_NAME_COST,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_COST,
            //    uiDesignConfiguration.UI_BTN_IMAGE_COST,
            //    UIDesignConfiguration.UI_BTN_HELP_COST,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_COST);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_TRADES,
            //    UIDesignConfiguration.UI_BTN_NAME_TRADES,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_TRADES,
            //    uiDesignConfiguration.UI_BTN_IMAGE_TRADES,
            //    UIDesignConfiguration.UI_BTN_HELP_TRADES,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_TRADES);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_MATERIALS,
            //    UIDesignConfiguration.UI_BTN_NAME_MATERIALS,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_MATERIALS,
            //    uiDesignConfiguration.UI_BTN_IMAGE_MATERIALS,
            //    UIDesignConfiguration.UI_BTN_HELP_MATERIALS,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_MATERIALS);

            //AddSplitButton(panel, "SplitButtonData10","Strategy",getSettingPushButton(
            //    UIDesignConfiguration.UI_BTN_ID_TIME,
            //    UIDesignConfiguration.UI_BTN_NAME_TIME,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_TIME,
            //    uiDesignConfiguration.UI_BTN_IMAGE_TIME,
            //    UIDesignConfiguration.UI_BTN_HELP_TIME,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_TIME),
            //    getSettingPushButton(
            //    UIDesignConfiguration.UI_BTN_ID_COST,
            //    UIDesignConfiguration.UI_BTN_NAME_COST,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_COST,
            //    uiDesignConfiguration.UI_BTN_IMAGE_COST,
            //    UIDesignConfiguration.UI_BTN_HELP_COST,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_COST),
            //    getSettingPushButton(
            //    UIDesignConfiguration.UI_BTN_ID_TRADES,
            //    UIDesignConfiguration.UI_BTN_NAME_TRADES,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_TRADES,
            //    uiDesignConfiguration.UI_BTN_IMAGE_TRADES,
            //    UIDesignConfiguration.UI_BTN_HELP_TRADES,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_TRADES), 
            //    getSettingPushButton(
            //    UIDesignConfiguration.UI_BTN_ID_MATERIALS,
            //    UIDesignConfiguration.UI_BTN_NAME_MATERIALS,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_MATERIALS,
            //    uiDesignConfiguration.UI_BTN_IMAGE_MATERIALS,
            //    UIDesignConfiguration.UI_BTN_HELP_MATERIALS,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_MATERIALS));
        }

        private void AddModeling(RibbonPanel panel)
        {

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_VERTICAL_FRAME,
            //    UIDesignConfiguration.UI_BTN_NAME_VERTICAL_FRAME,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_VERTICAL_FRAME
            //    , uiDesignConfiguration.UI_BTN_IMAGE_VERTICAL_FRAME,
            //    UIDesignConfiguration.UI_BTN_HELP_VERTICAL_FRAME,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_VERTICAL_FRAME);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_HORIZONTAL_FRAME,
            //    UIDesignConfiguration.UI_BTN_NAME_HORIZONTAL_FRAME,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_HORIZONTAL_FRAME,
            //    uiDesignConfiguration.UI_BTN_IMAGE_HORIZONTAL_FRAME,
            //    UIDesignConfiguration.UI_BTN_HELP_HORIZONTAL_FRAME,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_HORIZONTAL_FRAME);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_WALL,
            //    UIDesignConfiguration.UI_BTN_NAME_WALL,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_WALL,
            //    uiDesignConfiguration.UI_BTN_IMAGE_WALL,
            //    UIDesignConfiguration.UI_BTN_HELP_WALL,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_WALL);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_ROOF,
            //    UIDesignConfiguration.UI_BTN_NAME_ROOF,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_ROOF,
            //    uiDesignConfiguration.UI_BTN_IMAGE_ROOF,
            //    UIDesignConfiguration.UI_BTN_HELP_ROOF,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_ROOF);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_DECO,
            //    UIDesignConfiguration.UI_BTN_NAME_DECO,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_DECO,
            //    uiDesignConfiguration.UI_BTN_IMAGE_DECO,
            //    UIDesignConfiguration.UI_BTN_HELP_DECO,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_DECO);

            //addPushButtonData(panel,
            //    UIDesignConfiguration.UI_BTN_ID_BRACING,
            //    UIDesignConfiguration.UI_BTN_NAME_BRACING,
            //    uiDesignConfiguration.AssemblyFullName,
            //    UIDesignConfiguration.UI_BTN_EXECUTOR_BRACING,
            //    uiDesignConfiguration.UI_BTN_IMAGE_BRACING,
            //    UIDesignConfiguration.UI_BTN_HELP_BRACING,
            //    UIDesignConfiguration.UI_BTN_HELP_URL_BRACING);

            addPushButtonData(panel,
                UIDesignConfiguration.UI_BTN_ID_FIX_PROPERTIES,
                UIDesignConfiguration.UI_BTN_NAME_FIX_PROPERTIES,
                uiDesignConfiguration.AssemblyFullName,
                UIDesignConfiguration.UI_BTN_EXECUTOR_FIX_PROPERTIES,
                uiDesignConfiguration.UI_BTN_IMAGE_FIX_PROPERTIES,
                UIDesignConfiguration.UI_BTN_HELP_FIX_PROPERTIES,
                UIDesignConfiguration.UI_BTN_HELP_URL_FIX_PROPERTIES);

            addPushButtonData(panel,
                UIDesignConfiguration.UI_BTN_ID_SHARED_PARAMETERS,
                UIDesignConfiguration.UI_BTN_NAME_SHARED_PARAMETERS,
                uiDesignConfiguration.AssemblyFullName,
                UIDesignConfiguration.UI_BTN_EXECUTOR_SHARED_PARAMETERS,
                uiDesignConfiguration.UI_BTN_IMAGE_SHARED_PARAMETERS,
                UIDesignConfiguration.UI_BTN_HELP_SHARED_PARAMETERS,
                UIDesignConfiguration.UI_BTN_HELP_URL_SHARED_PARAMETERS);

        }

        private void AddSplitButton(RibbonPanel panel, string spiltId, string text, params PushButtonData[] btns)
        {
            SplitButtonData sb1 = new SplitButtonData(spiltId, text);
            SplitButton sb = panel.AddItem(sb1) as SplitButton;
            foreach (PushButtonData pbtn in btns)
            {
                sb.AddPushButton(pbtn);
            }
        }

        private void AddRadioGroup(RibbonPanel panel, params ToggleButtonData[] toggleButtonDatas)
        {
            // add radio button group
            RadioButtonGroupData radioData = new RadioButtonGroupData("radioGroup");
            RadioButtonGroup radioButtonGroup = panel.AddItem(radioData) as RadioButtonGroup;

            foreach (ToggleButtonData btn in toggleButtonDatas)
            {
                radioButtonGroup.AddItem(btn);
            }
        }

        private void AddSlideOut(RibbonPanel panel, params PushButtonData[] btns)
        {
            panel.AddSlideOut();
            foreach (PushButtonData btn in btns)
            {
                panel.AddItem(btn);
            }
        }

        private void addPushButtonData(RibbonPanel panel, string id, string text, string assembly, string executor, Uri imagePath, string helpText, string helpLink)
        {
            //PushButtonData pushButtonData = new PushButtonData(id, text,
            //assembly, executor);// Assembly.GetExecutingAssembly().Location 取得目前這個dll檔的位置
            //// Set the large image shown on button
            //pushButtonData.LargeImage = new BitmapImage(imagePath);
            //// Set ToolTip and contextual help
            //pushButtonData.ToolTip = helpText;
            //pushButtonData.SetContextualHelp(new ContextualHelp(ContextualHelpType.Url, helpLink));
            panel.AddItem(getSettingPushButton(id, text, assembly, executor, imagePath, helpText, helpLink));
        }

        private PushButtonData getSettingPushButton(string id, string text, string assembly, string executor, Uri imagePath, string helpText, string helpLink)
        {
            PushButtonData pushButtonData = new PushButtonData(id, text,
            assembly, executor); // Assembly.GetExecutingAssembly().Location 取得目前這個dll檔的位置
            // Set the large image shown on button
            pushButtonData.LargeImage = new BitmapImage(imagePath);

            // Set ToolTip and contextual help
            //pushButtonData.ToolTip = helpText;
            //pushButtonData.SetContextualHelp(new ContextualHelp(ContextualHelpType.Url, helpLink));
            return pushButtonData;
        }

        private ToggleButtonData getSettingToggleButton(string id, string text, string assembly, string executor, Uri imagePath, string helpText, string helpLink)
        {
            ToggleButtonData tb = new ToggleButtonData(id,
                text, assembly, executor);
            tb.ToolTip = helpText;
            tb.LargeImage = new BitmapImage(imagePath);
            return tb;
        }

        public Result OnShutdown(UIControlledApplication application)
        {


            return Result.Succeeded;
        }


    }

    #region PopUp 測試
    // █外部指令ClassE1: 訊息視窗
    [Transaction(TransactionMode.Manual)]
    public class DevMessage : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            // 跳出訊息視窗顯示版本資訊
            //TaskDialog.Show("DevMessage", "IA Lab. 2014/12/16/ iLuxProby N2");

            Sending sForm = new Sending(commandData);
            sForm.Show();
            return Result.Succeeded;
        }
    }
    #endregion

    #region Check 測試
    // █外部指令ClassE1: 訊息視窗
    [Transaction(TransactionMode.Manual)]
    public class ECDiary : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            // 跳出訊息視窗顯示版本資訊
            //TaskDialog.Show("DevMessage", "IA Lab. 2014/12/16/ iLuxProby N2");

            DiaryForm sForm = new DiaryForm(commandData);
            sForm.Show();
            //TaskDialog.Show("DevMessage", "IA Lab. 2014/12/16/ iLuxProby N2");
            return Result.Succeeded;
        }
    }
    #endregion

    #region 測試
    // █外部指令ClassE1: 訊息視窗
    [Transaction(TransactionMode.Manual)]
    public class ParameterTesting : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            // 跳出訊息視窗顯示版本資訊
            //TaskDialog.Show("DevMessage", "IA Lab. 2014/12/16/ iLuxProby N2");
            //選取物件
            UIDocument uiDoc = commandData.Application.ActiveUIDocument;
            Reference rElement = uiDoc.Selection.PickObject(ObjectType.Element, "選取開窗物件");

            //取得資訊
            Element rWindow = uiDoc.Document.GetElement(rElement);

            StringBuilder sb = new StringBuilder();
            foreach (Element element in elements)
            {
                sb.AppendLine(GetElementParameterInformation(commandData, element));
            }
            TaskDialog.Show("ParameterTesting", sb.ToString());
            return Result.Succeeded;
        }

        private String GetElementParameterInformation(ExternalCommandData commandData, Element element)
        {
            // Format the prompt information string
            String prompt = "Show parameters in selected Element:";
            StringBuilder st = new StringBuilder();
            // iterate element's parameters
            foreach (Parameter para in element.Parameters)
            {
                st.AppendLine(GetParameterInformation(para));
            }
            // Give the user some information
            return st.ToString();
        }

        String GetParameterInformation(Parameter para)
        {
            string defName = para.Definition.Name + @"\t";
            // Use different method to get parameter data according to the storage type
            switch (para.StorageType)
            {
                case StorageType.Double:
                    //covert the number into Metric
                    defName += " : " + para.AsValueString();
                    break;
                case StorageType.ElementId:
                    //find out the name of the element
                    ElementId id = para.AsElementId();
                    if (id.IntegerValue >= 0)
                    {
                        
                    }
                    else
                    {
                        
                    }
                    break;
                case StorageType.Integer:
                    if (ParameterType.YesNo == para.Definition.ParameterType)
                    {
                        if (para.AsInteger() == 0)
                        {
                            defName += " : " + "False";
                        }
                        else
                        {
                            defName += " : " + "True";
                        }
                    }
                    else
                    {
                        defName += " : " + para.AsInteger().ToString();
                    }
                    break;
                case StorageType.String:
                    defName += " : " + para.AsString();
                    break;
                default:
                    defName = "Unexposed parameter.";
                    break;
            }
            return defName;
        }
    }
    #endregion



}
