﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CCompass
{
    class UIDesignConfiguration
    {
        public string AssemblyFullName { get { return Assembly.GetExecutingAssembly().Location; } }// 取得目前dll檔的完整路徑+檔名
        public string AssemblyPath { get { return Path.GetDirectoryName(AssemblyFullName); } }// 取得目前dll檔所在資料夾路徑
        public string AssemblyResources { get { return Path.GetDirectoryName(AssemblyFullName) + "\\Resources"; } }// 取得目前dll檔所在資料夾中的Resource資料夾路徑

        
        public static String UI_RIBBON_TAB { get { return "C-Compass"; } }

        #region Icon
        public static String UI_PANEL_LOGO { get { return "C-Compass"; } }
        public static String UI_BTN_ID_LOGO { get { return "PushButtonData00"; } }
        public static String UI_BTN_NAME_LOGO { get { return "C-Compass"; } }//結構柱大木(垂直構件)
        public static String UI_BTN_HELP_LOGO { get { return "C-Compass"; } }
        public static String UI_BTN_HELP_URL_LOGO { get { return "About Me"; } }
        public static String UI_BTN_EXECUTOR_LOGO { get { return "CCompass.ECParameterTesting"; } }
        public Uri UI_BTN_IMAGE_LOGO { get { return new Uri(AssemblyResources + @"\ccompass.png"); } }
        #endregion

        //Panel Modeling
        /*Modeling面板下面按鈕有->結構柱大木
         * V Frame、H Frame、Wall、Roof、Deco、Bracing*/
        #region Modeling面板下面按鈕有->結構柱大木
        public static String UI_PANEL_Modeling { get { return "Model"; } }

        public static String UI_BTN_ID_VERTICAL_FRAME { get { return "PushButtonData01"; } }
        public static String UI_BTN_NAME_VERTICAL_FRAME { get { return "V Frame"; } }//結構柱大木(垂直構件)
        public static String UI_BTN_HELP_VERTICAL_FRAME { get { return "V Frame"; } }
        public static String UI_BTN_HELP_URL_VERTICAL_FRAME { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_VERTICAL_FRAME { get { return "CCompass.DevMessage"; } }
        public Uri UI_BTN_IMAGE_VERTICAL_FRAME { get { return new Uri(AssemblyResources + @"\icon.png"); } }

        public static String UI_BTN_ID_HORIZONTAL_FRAME { get { return "PushButtonData02"; } }
        public static String UI_BTN_NAME_HORIZONTAL_FRAME { get { return "H Frame"; } }//結構柱大木(水平構件)
        public static String UI_BTN_HELP_HORIZONTAL_FRAME { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_HORIZONTAL_FRAME { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_HORIZONTAL_FRAME { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_HORIZONTAL_FRAME { get { return new Uri(AssemblyResources + @"\cc-icon.png"); } }

        public static String UI_BTN_ID_WALL { get { return "PushButtonData03"; } }
        public static String UI_BTN_NAME_WALL { get { return "Wall"; } }
        public static String UI_BTN_HELP_WALL { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_WALL { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_WALL { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_WALL { get { return new Uri(AssemblyResources + @"\cc-icon.png"); } }

        public static String UI_BTN_ID_ROOF { get { return "PushButtonData04"; } }
        public static String UI_BTN_NAME_ROOF { get { return "Roof"; } }
        public static String UI_BTN_HELP_ROOF { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_ROOF { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_ROOF { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_ROOF { get { return new Uri(AssemblyResources + @"\cc-icon.png"); } }

        public static String UI_BTN_ID_DECO { get { return "PushButtonData05"; } }
        public static String UI_BTN_NAME_DECO { get { return "Deco"; } }
        public static String UI_BTN_HELP_DECO { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_DECO { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_DECO { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_DECO { get { return new Uri(AssemblyResources + @"\cc-icon.png"); } }

        public static String UI_BTN_ID_BRACING { get { return "PushButtonData06"; } }
        public static String UI_BTN_NAME_BRACING { get { return "Bracing"; } }
        public static String UI_BTN_HELP_BRACING { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_BRACING { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_BRACING { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_BRACING { get { return new Uri(AssemblyResources + @"\cc-icon.png"); } }

        public static String UI_BTN_ID_FIX_PROPERTIES { get { return "PushButtonData07"; } }
        public static String UI_BTN_NAME_FIX_PROPERTIES { get { return "Fix Model"; } }
        public static String UI_BTN_HELP_FIX_PROPERTIES { get { return " "; } }
        public static String UI_BTN_HELP_URL_FIX_PROPERTIES { get { return " "; } }
        public static String UI_BTN_EXECUTOR_FIX_PROPERTIES { get { return "CCompass.DevMessage"; } }
        public Uri UI_BTN_IMAGE_FIX_PROPERTIES { get { return new Uri(AssemblyResources + @"\fix properties.png"); } }

        public static String UI_BTN_ID_SHARED_PARAMETERS { get { return "PushButtonData08"; } }
        public static String UI_BTN_NAME_SHARED_PARAMETERS { get { return "Shared Parameters"; } }
        public static String UI_BTN_HELP_SHARED_PARAMETERS { get { return " "; } }
        public static String UI_BTN_HELP_URL_SHARED_PARAMETERS { get { return " "; } }
        public static String UI_BTN_EXECUTOR_SHARED_PARAMETERS { get { return "CCompass.DevMessage"; } }
        public Uri UI_BTN_IMAGE_SHARED_PARAMETERS { get { return new Uri(AssemblyResources + @"\shared parameters.png"); } }

        #endregion

        //Panel Designing   
        #region Strategy面板下面按鈕有、Time、Cost、Trades、Materials
        public static String UI_PANEL_DESIGNING { get { return "Strategy"; } }

        public static String UI_BTN_ID_TIME { get { return "PushButtonData10"; } }
        public static String UI_BTN_NAME_TIME { get { return "Time"; } }
        public static String UI_BTN_HELP_TIME { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_TIME { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_TIME { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_TIME { get { return new Uri(AssemblyResources + @"\2-1.png"); } }

        public static String UI_BTN_ID_COST { get { return "PushButtonData11"; } }
        public static String UI_BTN_NAME_COST { get { return "Cost"; } }
        public static String UI_BTN_HELP_COST { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_COST { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_COST { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_COST { get { return new Uri(AssemblyResources + @"\2-2.png"); } }

        public static String UI_BTN_ID_TRADES { get { return "PushButtonData12"; } }
        public static String UI_BTN_NAME_TRADES { get { return "Trades"; } }
        public static String UI_BTN_HELP_TRADES { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_TRADES { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_TRADES { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_TRADES { get { return new Uri(AssemblyResources + @"\2-3.png"); } }

        public static String UI_BTN_ID_MATERIALS { get { return "PushButtonData13"; } }
        public static String UI_BTN_NAME_MATERIALS { get { return "Materials"; } }
        public static String UI_BTN_HELP_MATERIALS { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_MATERIALS { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_MATERIALS { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_MATERIALS { get { return new Uri(AssemblyResources + @"\3-1.png"); } }

        public static String UI_BTN_ID_METHODS { get { return "PushButtonData14"; } }
        public static String UI_BTN_NAME_METHODS { get { return "History"; } }
        public static String UI_BTN_HELP_METHODS { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_METHODS { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_METHODS { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_METHODS { get { return new Uri(AssemblyResources + @"\methods.png"); } }

        public static String UI_BTN_ID_ASSEMBLING { get { return "PushButtonData15"; } }
        public static String UI_BTN_NAME_ASSEMBLING { get { return "Assemble"; } }
        public static String UI_BTN_HELP_ASSEMBLING { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_ASSEMBLING { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_ASSEMBLING { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_ASSEMBLING { get { return new Uri(AssemblyResources + @"\assembling.png"); } }
        
        #endregion

        //Panel Constructing 
        #region Constructing面板下面按鈕有Compose、Sending
        public static String UI_PANEL_CONSTRUCTING { get { return "Communication"; } }

        public static String UI_BTN_ID_COMPOSE { get { return "PushButtonData20"; } }
        public static String UI_BTN_NAME_COMPOSE { get { return "Notice"; } }
        public static String UI_BTN_HELP_COMPOSE { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_COMPOSE { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_COMPOSE { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_COMPOSE { get { return new Uri(AssemblyResources + @"\compose.png"); } }

        public static String UI_BTN_ID_SENDING { get { return "PushButtonData21"; } }
        public static String UI_BTN_NAME_SENDING { get { return "Send"; } }
        public static String UI_BTN_HELP_SENDING { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_SENDING { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_SENDING { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_SENDING { get { return new Uri(AssemblyResources + @"\sending.png"); } }

        #endregion

        //Panel Recording
        /**/
        #region Recording -> Weekly Statement(報表)、Statistics(統計)
        public static String UI_PANEL_RECORDING { get { return "Record"; } }

        //public static String UI_BTN_ID_STATEMENT { get { return "PushButtonData22"; } }
        //public static String UI_BTN_NAME_STATEMENT { get { return "Statement"; } }
        //public static String UI_BTN_HELP_STATEMENT { get { return "help Me"; } }
        //public static String UI_BTN_EXECUTOR_STATEMENT { get { return "CCompass.DevMessage"; } }
        //public static String UI_BTN_HELP_URL_STATEMENT { get { return "help Me"; } }
        //public Uri UI_BTN_IMAGE_STATEMENT { get { return new Uri(AssemblyResources + @"\4-3.png"); } }

        public static String UI_BTN_ID_STATISTICS { get { return "PushButtonData23"; } }
        public static String UI_BTN_NAME_STATISTICS { get { return "Statistics"; } }
        public static String UI_BTN_HELP_STATISTICS { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_STATISTICS { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_STATISTICS { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_STATISTICS { get { return new Uri(AssemblyResources + @"\4-4.png"); } }

        public static String UI_BTN_ID_CHECKING_UPDATE { get { return "PushButtonData24"; } }
        public static String UI_BTN_NAME_CHECKING_UPDATE { get { return "Check Update"; } }
        public static String UI_BTN_HELP_CHECKING_UPDATE { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_CHECKING_UPDATE { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_CHECKING_UPDATE { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_CHECKING_UPDATE { get { return new Uri(AssemblyResources + @"\checking update.png"); } }

        public static String UI_BTN_ID_PROGRESS { get { return "PushButtonData25"; } }
        public static String UI_BTN_NAME_PROGRESS { get { return "Gantt"; } }
        public static String UI_BTN_HELP_PROGRESS { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_PROGRESS { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_PROGRESS { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_PROGRESS { get { return new Uri(AssemblyResources + @"\progress.png"); } }

        public static String UI_BTN_ID_STORAGE { get { return "PushButtonData26"; } }
        public static String UI_BTN_NAME_STORAGE { get { return "Management"; } }
        public static String UI_BTN_HELP_STORAGE { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_STORAGE { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_STORAGE { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_STORAGE { get { return new Uri(AssemblyResources + @"\storage.png"); } }
            
            
        #endregion

        //Panel Report
        #region List明細表、Daily Statement施工日報表、Weekly Statement施工周報表、Monthly Statement施工月報表、Report施工記錄報告書
        public static String UI_PANEL_REPORT { get { return "Schedule"; } }

        public static String UI_BTN_ID_DAILY_STATEMENT { get { return "PushButtonData31"; } }
        public static String UI_BTN_NAME_DAILY_STATEMENT { get { return "Daily Statement"; } }
        public static String UI_BTN_HELP_DAILY_STATEMENT { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_DAILY_STATEMENT { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_DAILY_STATEMENT { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_DAILY_STATEMENT { get { return new Uri(AssemblyResources + @"\oneday.png"); } }

        public static String UI_BTN_ID_WEEKLY_STATEMENT { get { return "PushButtonData32"; } }
        public static String UI_BTN_NAME_WEEKLY_STATEMENT { get { return "Weekly Statement"; } }
        public static String UI_BTN_HELP_WEEKLY_STATEMENT { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_WEEKLY_STATEMENT { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_WEEKLY_STATEMENT { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_WEEKLY_STATEMENT { get { return new Uri(AssemblyResources + @"\week.png"); } }

        public static String UI_BTN_ID_MONTHLY_STATEMENT { get { return "PushButtonData33"; } }
        public static String UI_BTN_NAME_MONTHLY_STATEMENT { get { return "Monthly Statement"; } }
        public static String UI_BTN_HELP_MONTHLY_STATEMENT { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_MONTHLY_STATEMENT { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_MONTHLY_STATEMENT { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_MONTHLY_STATEMENT { get { return new Uri(AssemblyResources + @"\month.png"); } }

        public static String UI_BTN_ID_REPORT { get { return "PushButtonData34"; } }
        public static String UI_BTN_NAME_REPORT { get { return "Report"; } }
        public static String UI_BTN_HELP_REPORT { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_REPORT { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_REPORT { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_REPORT { get { return new Uri(AssemblyResources + @"\report.jpg"); } }

        public static String UI_BTN_ID_LIST { get { return "PushButtonData35"; } }
        public static String UI_BTN_NAME_LIST { get { return "List"; } }
        public static String UI_BTN_HELP_LIST { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_LIST { get { return "CCompass.DevMessage"; } }
        public static String UI_BTN_HELP_URL_LIST { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_LIST { get { return new Uri(AssemblyResources + @"\list.png"); } }

        public static String UI_BTN_ID_STATEMENT { get { return "PushButtonData36"; } }
        public static String UI_BTN_NAME_STATEMENT { get { return "Diary"; } }
        public static String UI_BTN_HELP_STATEMENT { get { return "help Me"; } }
        public static String UI_BTN_EXECUTOR_STATEMENT { get { return "CCompass.ECDiary"; } }
        public static String UI_BTN_HELP_URL_STATEMENT { get { return "help Me"; } }
        public Uri UI_BTN_IMAGE_STATEMENT { get { return new Uri(AssemblyResources + @"\diary.png"); } }


        #endregion


    }
}
